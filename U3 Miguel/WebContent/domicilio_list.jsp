<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Domicilio list</title>
</head>
<body>
<table border="1">
		<tr>
			<th>
			  <form action="DomicilioController">
			  		<input type="submit" name="btn_new"
			  		value="New "/>
			  </form>
			  <a href="Report">Print report</a>
			</th>
			<td>Id</td>
			<td>Calle</td>
			<td>Numero</td>
			<td>Colonia</td>
		</tr>
		 <c:forEach var="domicilio" items="${domicilios}">
		 	 <tr>
		 	 	<td>
		 	 		<form action="DomicilioController">
		 	 			<input type="hidden" name="id" value="${domicilio.id }">
		 	 			<input type="submit" name="btn_edit" value="Edit"/>
		 	 			 <input type="submit" name="btn_delete" value="Delete"/>
		 	 		</form>
		 	 	</td>
		 	 	<td>${domicilio.id }</td>
		 	 	<td>${domicilio.calle }</td>
		 	 	<td>${domicilio.numero }</td>
		 	 	<td>${domicilio.colonia }</td>
		 	 </tr>	
		 </c:forEach>
	</table>
</body>
</html>