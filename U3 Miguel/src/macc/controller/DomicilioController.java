package macc.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import macc.dao.DomicilioDAO;
import macc.dao.DomicilioDAOImpl;
import macc.model.Domicilio;


/**
 * Servlet implementation class StudentController
 */
@WebServlet("/DomicilioController")
public class DomicilioController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Domicilio domicilio;
	private List<Domicilio> domicilios;
	private DomicilioDAO domicilioDAO;
	private List<String> ids = new ArrayList<String>();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DomicilioController() {
        super();
      domicilio = new Domicilio();  
      domicilios = new ArrayList<Domicilio>();
      domicilioDAO = new DomicilioDAOImpl();
     
		
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (request.getParameter("btn_save") != null) {
			domicilio.setCalle(request.getParameter("calle"));
			domicilio.setNumero(request.getParameter("numero"));
			domicilio.setColonia(request.getParameter("colonia"));
			System.out.println("save");
			if (domicilio.getId()==0L) {
				System.out.println("save");		
				domicilioDAO.createDomicilio(domicilio);
			} else {
				System.out.println("save");
				domicilioDAO.updateDomicilio(domicilio);
			
			}
			domicilios = domicilioDAO.readAllDomicilios();

			request.setAttribute("domicilios", domicilios);
			request.getRequestDispatcher("domicilio_list.jsp").forward(request, response);
		}else if(request.getParameter("btn_new")!=null) {
			domicilio = new Domicilio();

			request.getRequestDispatcher("domicilio_form.jsp").forward(request, response);
		}else if(request.getParameter("btn_edit")!=null) {
			try {
				Long id = Long.parseLong(request.getParameter("id"));
				domicilio = domicilioDAO.readDomicilio(id);

			}catch (IndexOutOfBoundsException e) {
				domicilio = new Domicilio();

			}
		 request.setAttribute("domicilio", domicilio);
		 request.getRequestDispatcher("domicilio_form.jsp").forward(request, response);
		}else if(request.getParameter("btn_delete")!=null) {
			try {
				Long id = Long.parseLong(request.getParameter("id"));
				domicilioDAO.deleteDomicilio(id);
				domicilios = domicilioDAO.readAllDomicilios();

			}catch (Exception e) {
				e.printStackTrace();
			}
			request.setAttribute("domicilios", domicilios);
			request.getRequestDispatcher("domicilio_list.jsp").forward(request, response);
		}else {
			domicilios = domicilioDAO.readAllDomicilios();
			request.setAttribute("domicilios", domicilios);
			request.getRequestDispatcher("domicilio_list.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
