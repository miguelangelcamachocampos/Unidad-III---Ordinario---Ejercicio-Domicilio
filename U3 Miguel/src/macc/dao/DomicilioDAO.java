package macc.dao;

import java.util.List;

import macc.model.Domicilio;;

public interface DomicilioDAO {
	void createDomicilio(Domicilio domicilio);
	Domicilio readDomicilio(Long id);
	List<Domicilio> readAllDomicilios();
	void updateDomicilio(Domicilio domicilio);
	void deleteDomicilio(Long id);
	
}
