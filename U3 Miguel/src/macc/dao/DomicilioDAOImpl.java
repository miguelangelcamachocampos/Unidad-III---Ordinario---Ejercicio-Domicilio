/**
 * 
 */
package macc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import macc.model.Domicilio;;

/**
 * @author Miguel
 *
 */
public class DomicilioDAOImpl implements DomicilioDAO {
	private Connection connection;
	private PreparedStatement prepareStatement;
	private ResultSet resultSet;

	public  DomicilioDAOImpl() {
		getConnection();
	}

	public Connection getConnection() {
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "utng", "mexico");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}

	@Override
	public void createDomicilio(Domicilio domicilio) {
		if (connection != null) {
			try {
				prepareStatement = connection
						.prepareStatement("INSERT INTO domicilio (calle,numero,colonia) values(?,?,?);");
				prepareStatement.setString(1, domicilio.getCalle());
				prepareStatement.setString(2, domicilio.getNumero());
				prepareStatement.setString(3, domicilio.getColonia());
				prepareStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Domicilio readDomicilio(Long id) {
		Domicilio domicilio = null;
		if (connection != null) {
			try {
				prepareStatement = connection.prepareStatement("SELECT * FROM domicilio WHERE id=?;");
				prepareStatement.setLong(1, id);
				resultSet = prepareStatement.executeQuery();
				if (resultSet.next()) {
					domicilio = new Domicilio(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
							resultSet.getString(4));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return domicilio;
	}

	@Override
	public List<Domicilio> readAllDomicilios() {
		List<Domicilio> domicilios = new ArrayList<Domicilio>();
		if (connection != null) {
			try {
				prepareStatement = connection.prepareStatement("SELECT * FROM domicilio;");
				resultSet = prepareStatement.executeQuery();
				while (resultSet.next()) {
					Domicilio domicilio = new Domicilio(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3),
							resultSet.getString(4));
					domicilios.add(domicilio);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return domicilios;
	}

	@Override
	public void updateDomicilio(Domicilio domicilio) {
		if (connection != null) {
			try {
				prepareStatement = connection
						.prepareStatement("UPDATE domicilio  SET calle = ?, numero=?," + " colonia=?  WHERE id=?;");
				prepareStatement.setString(1, domicilio.getCalle());
				prepareStatement.setString(2, domicilio.getNumero());
				prepareStatement.setString(3,  domicilio.getColonia());
				prepareStatement.setLong(4, domicilio.getId());
				prepareStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void deleteDomicilio(Long id) {
		if (connection != null) {
			try {
				prepareStatement = connection.prepareStatement("DELETE FROM domicilio  WHERE id=?;");
				prepareStatement.setLong(1, id);
				prepareStatement.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
