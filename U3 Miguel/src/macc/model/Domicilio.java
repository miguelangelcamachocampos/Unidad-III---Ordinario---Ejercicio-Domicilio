/**
 * 
 */
package macc.model;

import java.io.Serializable;

/**
 * @author usuario
 *
 */
public class Domicilio implements Serializable{
	private Long id;
	private String calle;
	private String numero;
	private String colonia;
	/**
	 * @return the id
	 */
	public Domicilio (Long id, String calle, String numero, String colonia) {
		super();
		this.id = id;
		this.calle = calle;
		this.numero = numero;
		this.colonia = colonia;
	}
	
	public Domicilio() {
		this(0L, "", "","");
	}
	
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the calle
	 */
	public String getCalle() {
		return calle;
	}
	/**
	 * @param calle the calle to set
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	/**
	 * @return the colonia
	 */
	public String getColonia() {
		return colonia;
	}
	/**
	 * @param colonia the colonia to set
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	
	
}
